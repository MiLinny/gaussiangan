A toy example to practise TensorFlow and implementing a Generative Adversarial Network (GAN).

Trains a generator to simulate a normal distribution.

Based on the paper: https://arxiv.org/abs/1406.2661
